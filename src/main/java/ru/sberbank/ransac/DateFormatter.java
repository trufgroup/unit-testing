package ru.sberbank.ransac;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Утилитный класс, позволяющий работать с различными форматами дат
 */
public class DateFormatter {

    /**
     * 2016-01-02T00:00:00+0530
     */
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

    /**
     * 2016-01-30
     */
    public static final String SHORT_DATE_FORMAT = "yyyy-MM-dd";

    /**
     * 2016-01
     */
    public static final String MONTH_FORMAT = "yyyy-MM";


    /**
     * 01.2016
     */
    public static final String NEW_MONTH_FORMAT = "MM.yyyy";

    /**
     * 14:12:25+0530
     */
    public static final String TIME_FORMAT = "HH:mm:ssZ";

    /**
     * 14:12
     */
    public static final String NEW_TIME_FORMAT = "HH:mm";

    private DateFormatter() {
    }

    /**
     * Пытается распарсить значение в соответствующем формате.
     * Используется текущая TimeZone
     *
     * @param value  строка которая приходи к нам от сервера
     * @param format формат из которого нам надо распарсить
     * @return объект типа Date, с которым мы будем работать в филдах
     */
    public static Date parse( String value, String format) {
        return parse(value, format, TimeZone.getDefault());
    }

    /**
     * Пытается распарсить значение в соответствующем формате.
     * Используется TimeZone которая переданна параметром в метод
     *
     * @param value    строка которая приходи к нам непосредственно от сервера
     * @param format   формат из которого нам надо распарсить
     * @param timeZone временная зона, к которой мы будем приводить возвращаемый результат
     * @return объект типа Date, с которым мы будем работать в филдах
     */
    public static Date parse(String value, String format, TimeZone timeZone) {
        Date result = null;
        try {
            DateFormat dateFormat = new SimpleDateFormat(format);
            dateFormat.setTimeZone(timeZone);
            result = dateFormat.parse(value);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Пытается сформировать строку из значение в соответствующем формате.
     * Используется текущая TimeZone
     *
     * @param date   объект типа Date, получаем его из филдов, который работают с UI
     * @param format формат строки в которую мы будем переводить объект date
     * @return строка, формата, с которой работает сервер
     */
    public static String format(Date date, String format) {
        return format(date, format, TimeZone.getDefault());
    }

    /**
     * Пытается сформировать строку из значение в соответствующем формате.
     * Используется TimeZone которая переданна параметром в метод
     *
     * @param date     объект типа Date, получаем его из филдов, который работают с UI
     * @param format   формат строки в которую мы будем переводить объект date
     * @param timeZone временнная зона, из которой мы переводим объект date, перед отправкой на сервер
     * @return строка, формата, с которой работает сервер
     */
    public static String format(Date date, String format, TimeZone timeZone) {
        String result;
        DateFormat parserDateFormat = new SimpleDateFormat(format);
        parserDateFormat.setTimeZone(timeZone);
        result = parserDateFormat.format(date);
        return result;
    }
}
